terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Create a VPC
resource "aws_vpc" "danielf-vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "danielf-vpc"
  }
}

# Create a Subnet
resource "aws_subnet" "danielf-subnet" {
  vpc_id     = aws_vpc.danielf-vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "danielf-subnet"
  }
}

resource "aws_subnet" "danielf-subnet2" {
  vpc_id     = aws_vpc.danielf-vpc.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "danielf-subnet2"
  }
}

# Create an Internet Gateway
resource "aws_internet_gateway" "danielf-gw" {
  vpc_id = aws_vpc.danielf-vpc.id

  tags = {
    Name = "danielf-gw"
  }
}

# Create a Route-Table
resource "aws_route_table" "danielf-rtb" {
  vpc_id = aws_vpc.danielf-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.danielf-gw.id
  }
}

# Create a Route-Table Association
resource "aws_route_table_association" "danielf-rtb-a" {
  subnet_id      = aws_subnet.danielf-subnet.id
  route_table_id = aws_route_table.danielf-rtb.id
}

# Create a Security Group
resource "aws_security_group" "danielf-sg" {
  name   = "danielf-sg"
  vpc_id = aws_vpc.danielf-vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "danielf-sg"
  }
}

# Create an EC2 Instance
resource "aws_instance" "danielf-instance" {
  ami                    = "ami-0d5eff06f840b45e9"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.danielf-sg.id]
  subnet_id              = aws_subnet.danielf-subnet.id

  tags = {
    Name = "danielf-instance"
  }
}
